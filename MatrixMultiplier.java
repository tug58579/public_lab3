package lab3;

import java.util.List;

public abstract class MatrixMultiplier {
	
	abstract public int[][] multiply(int[][] A, int[][] B);
	
	// multiplies two matrices with timing information
    public int[][] runTest(int[][] A, int[][] B) {
    	System.out.println("Result using '" + this.getName() + "' class: ");
    	long startTime = System.currentTimeMillis();
    	int[][] R = this.multiply(A, B);
    	long endTime = System.currentTimeMillis();
    	System.out.println("Execution time: " + (endTime - startTime));
    	return R;
    }
    
    // calls runTest once for each multiplier
    public static int[][] runTestOnAll(List<MatrixMultiplier> multipliers, int[][] A, int[][] B) throws Exception {
    	int[][] result = null;
    	for(MatrixMultiplier multiplier : multipliers) {
    		if(result == null) {
    			result = multiplier.runTest(A, B);
    		}
    		else if(!MatrixFunctions.checkEquals(result, multiplier.runTest(A, B))) {
    			throw new Exception("Output matrices don't match");
    		}
    	}
    	System.out.println();
    	return result;
    }
    
    // returns a name to associate with this multiplier
    public String getName() {
		return this.getClass().getName();
	}
    
}
