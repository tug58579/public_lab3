package lab3;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
    	// compares the performance of NaiveMatrixMultiplier and BetterMatrixMultiplier
//    	test1();
    	
    	// compares the performance of NaiveMatrixMultiplier and RemoteMatrixMultiplier
    	 test2();
    	
    	// compares the performance of NaiveMatrixMultiplier, BetterMatrixMultiplier, and MultithreadedMatrixMultiplier
    	// test3();
    }
    
    public static void test1() throws Exception {
    	// matrix dimensions will be N by N
        final int N = 500;
        System.out.println("***** Multiplying " + N + " by " + N + " matrices *****");
        
        // create N by N matrices with random entries
        int[][] A = MatrixFunctions.createRandomMatrix(N, N);
        int[][] B = MatrixFunctions.createRandomMatrix(N, N);
        
        // create list of multipliers
        List<MatrixMultiplier> multipliers = new ArrayList<MatrixMultiplier>();
        multipliers.add(new NaiveMatrixMultiplier());
        multipliers.add(new BetterMatrixMultiplier());
        
        // executes multiplication for each multiplier and compares results
        MatrixMultiplier.runTestOnAll(multipliers, A, B);
    }
    
    public static void test2() throws Exception {
    	// matrix dimensions will be N by N
        final int N = 5;
        System.out.println("***** Multiplying " + N + " by " + N + " matrices *****");
        
        // create N by N matrices with random entries
        int[][] A = MatrixFunctions.createRandomMatrix(N, N);
        int[][] B = MatrixFunctions.createRandomMatrix(N, N);
        
        // create list of multipliers
        List<MatrixMultiplier> multipliers = new ArrayList<MatrixMultiplier>();
        multipliers.add(new NaiveMatrixMultiplier());
        multipliers.add(new RemoteMatrixMultiplier());
        
        // executes multiplication for each multiplier and compares results
        int[][] R = MatrixMultiplier.runTestOnAll(multipliers, A, B);
        
        // print matrices
        MatrixFunctions.printMatrix(A);
        MatrixFunctions.printMatrix(B);
        MatrixFunctions.printMatrix(R);
    }
    
    public static void test3() throws Exception {
    	// matrix dimensions will be N by N
        final int N = 1000;
        System.out.println("***** Multiplying " + N + " by " + N + " matrices *****");
        
        // create N by N matrices with random entries
        int[][] A = MatrixFunctions.createRandomMatrix(N, N);
        int[][] B = MatrixFunctions.createRandomMatrix(N, N);
        
        // create list of multipliers
        List<MatrixMultiplier> multipliers = new ArrayList<MatrixMultiplier>();
        multipliers.add(new NaiveMatrixMultiplier());
        multipliers.add(new BetterMatrixMultiplier());
        multipliers.add(new MultithreadedMatrixMultiplier(2));
        multipliers.add(new MultithreadedMatrixMultiplier(3));
        multipliers.add(new MultithreadedMatrixMultiplier(4));
        
        // executes multiplication for each multiplier and compares results
        MatrixMultiplier.runTestOnAll(multipliers, A, B);
    }

}
