package lab3;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
//import java.util.StringTokenizer;

public class RemoteMatrixMultiplier extends MatrixMultiplier {

	// ADD HELPER METHODS HERE
	
	@Override
	public int[][] multiply(int[][] A, int[][] B) {
		// dimensions
		int rowsA = A.length;
        int colsB = B[0].length;

        // matrices
        int[][] product = new int[rowsA][colsB];
        int[][] transposeB = MatrixFunctions.getTranspose(B);
        
        // nested for loop
        for (int i = 0; i < rowsA; i++) {
            for (int j = 0; j < colsB; j++) {
//            	product[i][j] = VectorFunctions.dot(A[i], transposeB[j]);
            	//MAKE CALL TO PHP PAGE
            	try {
					product[i][j] = sendGetRequest(A[i], transposeB[j]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
        
        
        
        return product;
	}
	
	// ADD HELPER METHODS HERE
	public static int sendGetRequest(int[] A, int[] B) throws Exception {
		String aMatrix = Arrays.toString(A);
		aMatrix = aMatrix.replaceAll(", ", " ").replace("[", "").replace("]", "");
		
		String bMatrix = Arrays.toString(B);
		bMatrix = bMatrix.replaceAll(", ", " ").replace("[", "").replace("]", "");
		
		// building a url string with parameters
        String base_url = "http://cis-linux2.temple.edu/~tug72675/mult.php";
        String parameters = "a=" + URLEncoder.encode(aMatrix, "UTF-8") + "&b=" + URLEncoder.encode(bMatrix, "UTF-8");
        String url = base_url + "?" + parameters;

        // send http get request
        URL Url = new URL(url);
        HttpURLConnection con = (HttpURLConnection) Url.openConnection();
        con.setRequestMethod("GET");
//        System.out.println("Sending http 'GET' request to URL : " + url);
        
        // get & print response
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
//        System.out.println(response.toString());
        
        return Integer.parseInt(response.toString());
    }

}
