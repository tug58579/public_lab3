package lab3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RunnerTest {

	public static void concatenate(String[] results, int index, String input1, String input2) {
		results[index] = input1 + " " + input2;
	}
	
	public static void main(String[] args) {
		// define references to results
		String[] results = new String[3];
		
		// create a list of runnables
		List<Runnable> runnables = new ArrayList<Runnable>();
		runnables.add(new Runnable() {
			public void run() {
				RunnerTest.concatenate(results, 0, "star", "wars");
			}
		});
		runnables.add(new Runnable() {
			public void run() {
				RunnerTest.concatenate(results, 1, "dream", "big");
			}
		});
		runnables.add(new Runnable() {
			public void run() {
				RunnerTest.concatenate(results, 2, "hello", "world");
			}
		});
		
		// execute all runnables in parallel
		try {
			Runner.runInParallel(runnables);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// print results
		System.out.println(Arrays.toString(results));
	}
	
}
