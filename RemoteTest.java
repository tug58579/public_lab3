package lab3;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class RemoteTest {

	// this method computes the dot product of (1,2,3,4,5) and (1,2,3,4,5)
    public static void sendGetRequest() throws Exception {
    	// building a url string with parameters
        String base_url = "http://cis-linux2.temple.edu/~tug72675/mult.php";
        String parameters = "a=" + URLEncoder.encode("1 2 3 4 5", "UTF-8") + "&b=" + URLEncoder.encode("1 2 3 4 5", "UTF-8");
        String url = base_url + "?" + parameters;

        // send http get request
        URL Url = new URL(url);
        HttpURLConnection con = (HttpURLConnection) Url.openConnection();
        con.setRequestMethod("GET");
        System.out.println("Sending http 'GET' request to URL : " + url);
        
        // get & print response
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        System.out.println(response.toString());
    }

    public static void main(String[] args) throws Exception {
        sendGetRequest();
    }

}
