package lab3;

import java.util.ArrayList;
import java.util.List;

public class Runner {

	public static void runInParallel(List<Runnable> runnables) throws InterruptedException {
		// create threads
		List<Thread> threads = new ArrayList<Thread>();
		for(Runnable runnable : runnables) {
			threads.add(new Thread(runnable));
		}
		
		// start threads
		for(Thread thread : threads) {
			thread.start();
		}
		
		// join threads
		for(Thread thread : threads) {
			thread.join();
		}
	}
	
}
