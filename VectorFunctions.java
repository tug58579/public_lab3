package lab3;

public class VectorFunctions {

	// compute vector dot product
    public static int dot(int[] v1, int[] v2) {
    	int sum = 0;
    	for(int i = 0; i < v1.length; i++) {
    		sum += v1[i] * v2[i];
    	}
    	return sum;
    }
    
    // converts a vector to a string
    public static String toString(int[] v) {
        StringBuilder builder = new StringBuilder();
        boolean firstIteration = true;
        for (int x : v) {
        	if(firstIteration) {
        		builder.append(x);
        		firstIteration = false;
        	}
        	else {
        		builder.append(" ").append(x);
        	}
        }
        return builder.toString();
    }
	
}
