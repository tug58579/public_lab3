package lab3;

public class MatrixFunctions {

    // create a matrix with random entries
    public static int[][] createRandomMatrix(int rows, int cols) {
    	int[][] M = new int[rows][cols];
        for (int i = 0; i < M.length; i++) {
            for (int j = 0; j < M[i].length; j++) {
                M[i][j] = (int) (Math.random() * 20);
            }
        }
        return M;
    }

    // print the entries of a matrix
    public static void printMatrix(int[][] M) {
        System.out.println(M.length + " x " + M[0].length);
        for (int i = 0; i < M.length; i++) {
            for (int j = 0; j < M[i].length; j++) {
            	System.out.print(M[i][j]);
            	if(j < M[i].length - 1) {
            		System.out.print("\t");
            	}
            }
            System.out.println();
        }
        System.out.println();
    }

    // compute matrix transpose
    public static int[][] getTranspose(int[][] M) {
		int[][] transposeM = new int[M[0].length][M.length];
    	for(int i = 0; i < M.length; i++) {
    		for(int j = 0; j < M[0].length; j++) {
    			transposeM[j][i] = M[i][j];
    		}
    	}
    	return transposeM;
	}
    
    // check if two matrices are equals
    public static boolean checkEquals(int[][] A, int[][] B) {
    	for(int i = 0; i < A.length; i++) {
    		for(int j = 0; j < A[0].length; j++) {
    			if(A[i][j] != B[i][j]) {
    				return false;
    			}
    		}
    	}
    	return true;
    }
	
}
