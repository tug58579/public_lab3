package lab3;

public class NaiveMatrixMultiplier extends MatrixMultiplier {
	
	@Override
	public int[][] multiply(int[][] A, int[][] B) {
		// dimensions
        int rowsA = A.length;
        int colsA = A[0].length;
        int colsB = B[0].length;
        
        // matrices
        int[][] product = new int[rowsA][colsB];
        
        // nested for loops
        for (int i = 0; i < rowsA; i++) {
            for (int j = 0; j < colsB; j++) {
                for (int k = 0; k < colsA; k++) {
                    product[i][j] += A[i][k] * B[k][j];
                }
            }
        }
        
        return product;
	}	

}
