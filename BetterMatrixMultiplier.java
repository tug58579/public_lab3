package lab3;

public class BetterMatrixMultiplier extends MatrixMultiplier {

	@Override
	public int[][] multiply(int[][] A, int[][] B) {
		// dimensions
		int rowsA = A.length;
        int colsB = B[0].length;

        // matrices
        int[][] product = new int[rowsA][colsB];
        int[][] transposeB = MatrixFunctions.getTranspose(B);
        
        // nested for loop
        for (int i = 0; i < rowsA; i++) {
            for (int j = 0; j < colsB; j++) {
            	product[i][j] = VectorFunctions.dot(A[i], transposeB[j]);
            }
        }
        
        return product;
	}

}
