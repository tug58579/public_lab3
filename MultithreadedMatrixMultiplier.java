package lab3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MultithreadedMatrixMultiplier extends MatrixMultiplier {

	final private int numOfThreads;

	public MultithreadedMatrixMultiplier(int numOfThreads) {
		this.numOfThreads = numOfThreads;
	}
	
	public static int calculate(int[][] product, int numRows, int numCol, int[][] A, int[][] transposeB, int numThreads, int threadID){
		int startRow = threadID * (numRows / numThreads);
		int endRow = (threadID + 1) * (numRows / numThreads) - 1;
		int lastThread = numThreads - 1;
		
		if(threadID == lastThread){
			endRow = numRows - 1;			
		}
		
		for (int i = startRow; i <= endRow; i++) {
            for (int j = 0; j < numCol; j++) {
            	product[i][j] = VectorFunctions.dot(A[i], transposeB[j]);
            }
        }

		return 0;
	}
	
	@Override
	public int[][] multiply(final int[][] A, int[][] B) {
		// dimensions
			final int rowsA = A.length;
	        final int colsB = B[0].length;
	        final int numThreads;

	        // matrices
	        final int[][] product = new int[rowsA][colsB];
	        final int[][] transposeB = MatrixFunctions.getTranspose(B);
	        
	        List<Runnable> runnables = new ArrayList<Runnable>();
	        
	        if(this.numOfThreads > rowsA){
	        	numThreads = rowsA;
	        }
	        else{
	        	numThreads = this.numOfThreads;
	        }
	        
	        
	        for(int i = 0; i < numThreads ; i++){	//Each thread
	        	final int threadID = i;
	       
	        	runnables.add(new Runnable() {
					public void run() {
						MultithreadedMatrixMultiplier.calculate(product, rowsA, colsB, A, transposeB, numThreads, threadID);
					
					}
				});
	        
	        }
	        
	    	// execute all runnables in parallel
			try {
				Runner.runInParallel(runnables);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	   
		return product;
	}
	
	// ADD HELPER METHODS HERE
	
	@Override
	public String getName() {
		return super.getName() + "(" + numOfThreads + ")";
	}

}
